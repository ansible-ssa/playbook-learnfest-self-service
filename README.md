# playbook-learnfest-self-service

This project contains Playbooks needed to provision student infrastructure for the [Ansible LearnFest](https://ansible-learnfest.gitlab.io/).

The student instructions have been moved to the [Ansible LearnFest Prepare Environment](https://ansible-learnfest.gitlab.io/10-prepare-environment/) web site.
